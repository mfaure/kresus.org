Title: Kresus 0.16
Date: 2020-04-18 13:00
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-16-0

Kresus est confiné, et [ça se voit sur nos
graphiques](https://kresus.org/blog/covid-19-analyse.html) ! Cinq mois depuis
la montée de version précédente, nous vous proposons aujourd'hui une version
avec plein de chouettes nouveautés, la 0.16 !

Si vous êtes pressé.e.s et ne désirez que l'essentiel, nous vous invitons à lire
au moins les notes de mises à jour pour connaître les
nouveaux besoins de Kresus !

## Nouvelles fonctionnalités

### Édition par lots

C'était une demande récurrente : la possibilité de modifier la catégorie, le
libellé, le type etc. de plusieurs opérations en un seul coup. *@sinopsysHK*
(que nous remercions chaudement !) l'a implémenté. Et oui, ça marche aussi
lorsque l'on fait une recherche !

<video width="100%" height="315"
    controls
    src="{static}/videos/kresus-16-bulk.mp4"
    allowfullscreen></video>

### Notifications via Apprise

Vous aimeriez recevoir des notifications, mais par d'autres biais que les
emails ? C'est désormais possible, grâce à l'intégration de la communication
avec [Apprise](https://github.com/caronc/apprise), qui donne la possibilité à
Kresus d'émettre des notifications via Telegram, Discord, Slack, Gotify
(open-source !), ou n'importe quel autre système compatible. Un grand merci à
*@Aerion* !

<video width="100%" height="315"
    controls
    src="{static}/videos/kresus-16-apprise.webm"
    allowfullscreen></video>

[En savoir plus]({filename}../pages/fr/03-user-doc.md#comment-puis-je-configurer-mes-notifications-avec-apprise).

### Support basique de l'authentification multi-facteurs (2FA)

Kresus supporte silencieusement l'authentification multi-facteurs, qui est en
train de se généraliser chez toutes les banques. Les banques qui nécessitent
une validation par application sont donc correctement supportées ; celles qui
vous envoient une information à vérifier (par exemple un code par texto) ne le
sont pas encore (mais ce sera pour une prochaine version !). Si le site de
votre banque ne demande une authentification multi-facteurs qu'une seule fois
tous les 90 jours, Kresus sera désormais capable d'en faire autant ! Et si vous
avez configuré vos emails, une alerte vous sera envoyée dés qu'il est
nécessaire de se reconnecter manuellement au site de votre banque.

### Tableau de bord

Un tableau de bord fait son apparition et vous montre l'intégralité de vos
comptes chez toutes vos banques, ainsi que le graphique des rentrées et sorties
pour ce mois, sur chaque accès bancaire. Pratique pour suivre l'évolution de
vos comptes en un clin d'oeil !

### Mais aussi...

- Kresus essaie désormais d'éviter la génération de certains doublons quand le
  type de l'opération (virement, carte bleue, etc.) est la seule chose qui
  change par rapport à ce que Kresus connaît déjà. Cela permet notamment de
  gérer correctement les cartes à débit différé, en transformant les opérations
  de carte différée en carte bleue à la fin du mois.
- Quand il existe des opérations futures ou des encours de carte, leur total
  est désormais affiché dans la liste des opérations.
- Il est désormais possible de voir la balance totale de tous les accès,
  incluant tous les comptes, dans le menu, ce qui vous permet d'avoir un coup
  d'oeil rapide sur la totalité de vos possessions !
- Il est maintenant possible de marquer des opérations comme étant des
  virements internes, afin que celles-ci n'apparaissent plus dans la plupart
  des graphiques. Pour se faire, changez le type d'une transaction vers
  "Virement interne".
- Lorsqu'on vous a demandé quel était votre thème préféré, la réponse fut
  limpide : 40% d'entres vous ignoraient que plusieurs thèmes étaient
  disponibles. On a simplifié tout ça et on vous permet maintenant de choisir
  entre un thème clair (*light*) et un thème sombre (*dark*). Les interfaces
  ont donc été repensées, et sont plus adaptées au contexte d'utilisation et
  plus modernes. Pour alterner entre les deux modes ça se passe dans
  *Personnalisation* depuis le menu en haut à droite !
- La page d'administration a été unifiée pour la gestion de weboob et des logs.
  Elle est désormais disponible dés la première utilisation, avant d'avoir créé
  un premier accès bancaire.
- Support de nouvelles banques, notamment HSBC Hong Kong (@sinopsysHK).
- La toupie d'attente (mais si, vous savez, le cercle qui tourne quand Kresus
  fait une opération qui prend du temps !) a hérité d'un nouveau look, conçu
  par @framasky.
- Le succès ou l'échec de plus d'opérations vous sont désormais explicitement
  signalés.
- Quand une recherche n'a pas de résultat, Kresus vous l'indique désormais.
- La recherche est désormais insensible aux accents : rechercher "impot"
  trouvera également "impôt", et vice et versa.
- Il est désormais possible d'affecter un budget de zéro euros à une catégorie.
  Bah pourquoi pas ?

### Et sous le capot

Pour accompagner cette migration vers SQL, nous avons également initié la
réécriture du serveur en Typescript. Cela nous permet principalement de typer
les variables et s'assurer une meilleure stabilité et une meilleure
maintenabilité du code.

L'intégration continue a été plusieurs fois réécrite pour tirer parti des
caches de Gitlab, ce qui la rend beaucoup plus rapide.

## Notes de mise à jour

Pour les sysadmins, devops et autres mainteneuses attentives, voici un résumé
des actions à effectuer pour mettre à jour vers cette version de Kresus.

### Migration vers SQL, l'infini et au-delà

Kresus n'utilise plus de base de données émulée à base de fichiers, mais
utilise désormais une base de données PostgreSQL comme stockage pour vos
données personnelles. En plus des gains en maintenabilité du code, cela veut
dire que nous pouvons officiellement dire merci et adieu à la base de données
émulée sous forme de fichiers. Notamment, toutes les actions serveur sont plus
rapides ! Chargement initial plus rapide, sauvegardes et import plus rapides,
etc.

Pour les instructions d'installation, veuillez vous référer à notre
[documentation](https://kresus.org/admin.html#base-de-donnees-sql). Une fois la
base de données configurée dans votre `config.ini`, la migration se fait toute
seule au sein de Kresus, pour cette version.

### Plus de CLI pour les nerds

L'application Kresus dispose désormais d'autres fonctions en ligne de commande,
pour vous aider dans l'administration de votre instance :

- `kresus create:config` va générer un fichier de configuration vide par
  défaut, toujours à jour avec la version de Kresus que vous utilisez.
- `kresus create:user $login` permet de créer un nouveau compte utilisateur.ice
  pour votre Kresus. L'identifiant du compte créé est renvoyé, et vous pouvez
  ensuite le renseigner dans le fichier de configuration `config.ini` pour
  l'instance en cours. Cela permet notamment d'utiliser une seule base de
  données et plusieurs instances de Kresus qui s'y connectent, en attendant
  d'avoir une gestion multi-utilisateurs dans Kresus.

### Voici l'authentification Basic, simple

Kresus n'incluant pas de système d'authentification, pour protéger vos données
de toute visualisation non voulue, il fallait jusque là passer par un *reverse
proxy* (comme nginx ou apache) pour demander un mot de passe pour accéder au
site. Grâce à une contribution de *@m4dz*, Kresus peut maintenant gérer cette
authentification directement, sans qu'il n'y ait plus besoin de le spécifier au
serveur front. Les prochaines versions apporteront une solution
multi-utilisateurs avec une authentification intégrée à l'application.

### Montées de version

Kresus nécessite désormais **Python 3** et **Weboob 2.0** au minimum pour fonctionner.

## Merci, sympa, sympa, merci

Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement (merci Clara en particulier !), pour
leurs retours, questions, encouragement, support, etc.

Merci aux mainteneurs du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code, notamment aux belles contributions de
[Framasky](https://framapiaf.org/@framasky), Aerion, SinopsysHK, et
[m4dz](https://twitter.com/m4d_z) !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !
