Title: Aidez-nous à tester l'outil de migration vers SQL !
Date: 2020-03-16 21:30
Lang: fr
Slug: tester-migration

Bonjour à tou.te.s !

Comme vous le savez peut-être, Kresus va bientôt utiliser une base de données
SQL pour fonctionner, ce qui permettra d'avoir des meilleures performances, et
à terme, de pouvoir héberger plusieurs personnes au sein d'un même Kresus.

Cependant, la migration entre l'ancien système et le nouveau est un peu
complexe, et malgré quelques tests de notre côté avec nos données, notre niveau
de certitudes quant à la réussite de la migration est un peu bas.

Nous aurions donc besoin de votre aide ! **Oui, vous !**

## De quoi vous avez besoin

- Des petites compétences techniques ! Pour cette fois-ci, il sera nécessaire
  de savoir manipuler la ligne de commande, installer des packages via npm, et
  bidouiller un petit peu. **Si vous avez réussi à installer Kresus par
  vous-mêmes, cela devrait être largement suffisant.**
- Un serveur avec un Kresus installé dessus.
- La capacité de prendre un peu de temps pour nous décrire votre problème, si
  vous en rencontrez un en cours de route :)

## Étapes

### Faire un backup de l'ancienne base

Votre ancienne base est sauvegardée sous forme de dossiers et de fichiers, dans
le répertoire indiqué par `datadir` dans votre fichier de configuration, ou
bien la variable d'environnement `KRESUS_DIR`, ou bien la valeur par défaut
(qui devrait être `~/.kresus` dans ce cas). Il est fortement conseillé de
sauvegarder l'intégralité de ce répertoire, par exemple en le zippant dans un
fichier de sauvegarde que vous mettrez en lieu sûr (un dossier synchronisé sur
le cloud, une clé USB, un disque dur externe, etc.).

Par exemple, pour une base située dans /home/ben/.kresus, je peux la
sauvegarder avec :

    :::bash
    zip /home/ben/lieu-sur/kresus-backup.zip -r /home/ben/.kresus

Et au cas où j'aurais besoin de la récupérer, un simple `unzip
/home/ben/lieu-sur/kresus-backup.zip` devrait suffire à extraire tous les
fichiers.

### Installer l'outil de test de migration

Avec la ligne de commande suivante, qui va effectuer une installation préfixée
dans un sous-répertoire de /tmp :

    :::bash
    npm install --production --prefix=/tmp kresus-export-cozydb


### Lancer l'outil de test de migration et vérifier

Avec la commande suivante, à laquelle vous passez le chemin vers votre fichier
de configuration Kresus (`config.ini`). La commande va créer le contenu de
sortie (un export JSON) dans /tmp/output.json :

    :::bash
    /tmp/node_modules/kresus-export-cozydb/bin/export.js -c config.ini -o /tmp/output.json

Si tout se passe bien, vous devriez voir des messages s'afficher dans le
terminal pour vous indiquer la progression, et cela devrait se terminer par un
message qui indique **"Succesfully exported data to /tmp/output.json!"**. Dans
ce cas, vous pouvez ouvrir le fichier en question (avec n'importe quel éditeur
de texte), et vérifier si les données semblent correspondre à ce que vous avez
dans votre Kresus. Pas besoin de tout vérifier manuellement, mais se concentrer
sur **quelques opérations** et vos **comptes**, ou encore les **identifiants et
paramètres** (est-ce que le choix d'un site web ou d'un champ spécial est bien
gardé, etc.) par exemple, peut suffire à se faire une bonne idée.

Si ce n'est pas le cas, vous avez une erreur, et il serait extrêmement génial
de votre part de nous la rapporter sur notre [gestionnaire de
tickets](https://framagit.org/kresusapp/kresus-export-cozydb/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
ou via [notre forum](https://community.kresus.org/). En premier lieu, nous
copier-coller l'erreur et les informations contextuelles pourrait suffire à
identifier le problème. Attendez-vous également à ce que l'on vous demande
éventuellement un extrait des données en question, mais tout ça pourra se faire
par message privé ou email, si besoin est, et on vous expliquera comment cacher
vos données personnelles à l'occasion.

### Merci !

Merci d'avoir lu jusqu'ici, et nous espérons lire vos retours dès que possible,
**même si c'est juste pour nous dire que tout s'est bien passé** (pourquoi pas
via les médias sociaux) ! Plus nous aurons de retours, plus nous aurons de
certitudes quant à la qualité de l'outil, et plus nous serons confiants dans
notre prochaine montée de version :-).
