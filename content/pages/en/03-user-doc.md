Title: How to use Kresus
Date: 2019-06-17 18:02
Slug: user-doc
Summary: Learn how to use Kresus
toc_run: true
toc_title: How to use Kresus?
lang: en
status: hidden
is_doc: true


### My account balance is incorrect in Kresus. What should I do?

This can happen from time to time has the underlying software used by Kresus,
Weboob, only gives an instantaneous view of the banking data from your bank
website, while Kresus tries to reconstruct the entire history. If data
provided by your bank are not coherent across time (some data change), then
the balance of one of your accounts might be incorrect in Kresus.

We advise you have a look at the "Duplicate" section in Kresus. If some
operations seem to be duplicates, this page will show them and you will be
able to merge them, by hand, to get back a correct balance. However, the way
of doing it is quite rudimentary and some operations marked as duplicates
could actually not be duplicates. It is not possible to cancel the merge of
two operations, hence take care when removing duplicates!

In the future, it will be possible to cancel the merge of duplicates.
Automatic detection and merge of duplicates should also be greatly improved.

Finally if the issue remains, in last resort, you can still force Kresus to resynchronize the
balance amount of the faulty account with the amount returned by your bank.
Go to *Settings* > *Bank accounts* and click on the gear icon on the line of the chosen account.

![Resync accountbutton](../images/pages/en/resync-account-icon.png)

A modale will ask you to confirm:

![Modale asking to confirm account resync](../images/pages/en/resync-confirm-modale.png)

### How can I manually add a transaction ?

It can sometimes be useful to create transactions before then even appear on
the bank's website and thus be fetched by Kresus.

To manually add a transaction, go to the transaction list and click on
”Add an operation” (“+” on mobile).

![Add transaction button](../images/pages/en/add-transaction-button.png)

If you create it on an account that is automatically updated through the
connection to your bank's website, once the real transaction appears on the
website and is retrieved by Kresus, a duplicate transaction should be
generated, that you'll have to manually merge. You might have to use the
“Find more” button if the date of the manual transaction is far enough from
the one of the actual transaction.

![Add transaction screen](../images/pages/en/add-transaction-modale.png)

## Can I import OFX data and other formats?

Kresus supports OFX format and its own, a JSON file with a `data` entry which contains:

* an `accesses` array with the following properties:
    * `id` (number): a unique id describing the access.
    * `vendorId` (string): the bank/backend identifier. Currently either `manual` or or one of the weboob bank vendor ids (`uuid`) in this list: [https://framagit.org/kresusapp/kresus/blob/master/shared/banks.json](https://framagit.org/kresusapp/kresus/blob/master/shared/banks.json) (e.g. n26, bnporc, etc.). If the bank access is not supported by weboob, you'll want to use a `manual` vendor.
    * `login` (string): your access login. The password cannot be set in this format, you'll have to enter it manually later in the interface.
    * `customLabel` (string/optional): a user-facing label for the access.
* an `accounts` array with the following properties:
    * `id` (number): a unique id describing the account.
    * `vendorAccountId` (string): a unique string describing the account.
    * `vendorId` (string): the bank/backend identifier. (yes you have to specify it here too for the moment).
    * `accessId` (number): a reference to the `id` property of an `accesses` entry.
    * `type` (string): the type of the account. One of the `name` values here: [https://framagit.org/kresusapp/kresus/blob/master/shared/account-types.json](https://framagit.org/kresusapp/kresus/blob/master/shared/account-types.json).
    * `initialBalance` (number): the initial balance of the account. If you do not have all the historical data you can specify the balance before all the operations you include so that the total matches your current balance.
    * `label` (string): the account label.
    * `iban` (string/optional): the account IBAN.
    * `currency` (string/optional): the account currency in ISO-4217 code.
* a `categories` array with the following properties:
    * `id` (number): a unique id describing the category.
    * `label` (string): the category label.
    * `color` (string/optional): the category color in hexadecimal format. If omitted a color will automatically be generated.
* an `operations` array with the following properties:
    * `accountId` (number): a reference to the `id` property of an `accounts` entry.
    * `categoryId` (number/optional): a reference to the `id` property of a `categories` entry.
    * `type` (string/optional): the transaction type. One of the `name` values from [https://framagit.org/kresusapp/kresus/blob/master/shared/operation-types.json](https://framagit.org/kresusapp/kresus/blob/master/shared/operation-types.json).
    * `rawLabel` (string): the transaction raw label (generally the full label)
    * `label` (string/optional): the transaction label (generally striped of metadata like date or type).
    * `customLabel` (string/optional): a custom label if you want something more clear (ex: "Food for my birthday").
    * `date` (string): the transaction's date in ISO format.
    * `debit_date` (string/optional): the transaction's debit_date (when the bank acknowledged the transaction) in ISO format.
    * `amount` (number): the transaction amount.

**Example**:

    :::json
    {
        "data": {
            "accesses": [
                {
                    "vendorId": "manual",
                    "login": "whatever-manual-acc--does-not-care",
                    "customLabel": "Optional custom label",
                    "id": 0
                }
            ],
            "accounts": [
                {
                    "id": 0,
                    "vendorId": "manual",
                    "accessId": 0,
                    "vendorAccountId": "manualaccount-randomid",
                    "type": "account-type.checking",
                    "initialBalance": 0,
                    "label": "Compte Courant",
                    "iban": "FR4830066645148131544778523",
                    "currency": "EUR"
                }
            ],
            "categories": [
                {
                    "label": "Groceries",
                    "color": "#1b9d68",
                    "id": 0
                },
                {
                    "label": "Books",
                    "color": "#b562bf",
                    "id": 1
                },
                {
                    "label": "Taxes",
                    "color": "#ff0000",
                    "id": 2
                },
                {
                    "label": "Misc",
                    "color": "#00ff00",
                    "id": 3
                }
            ],
            "operations": [
                {
                    "accountId": 0,
                    "categoryId": 0,
                    "type": "type.card",
                    "label": "Wholemart",
                    "rawLabel": "card 07/07/2019 wholemart",
                    "customLabel": "Food",
                    "date": "2019-07-07T00:00:00.000Z",
                    "amount": -83.8
                },
                {
                    "accountId": 0,
                    "categoryId": 0,
                    "type": "type.card",
                    "label": "Wholemart",
                    "rawLabel": "card 09/07/2019 wholemart",
                    "customLabel": "Food & stuff",
                    "date": "2019-07-09T00:00:00.000Z",
                    "amount": -60.8
                },
                {
                    "accountId": 0,
                    "categoryId": 1,
                    "type": "type.card",
                    "label": "amazon payments",
                    "rawLabel": "carte 19/07/2019 amazon payments",
                    "customLabel": "1984 - George Orwell",
                    "date": "2019-07-19T00:00:00.000Z",
                    "amount": -20
                },
                {
                    "accountId": 0,
                    "type": "type.transfer",
                    "label": "SEPA m. john doe 123457689 rent",
                    "rawLabel": "transfer to m. john doe 123457689 rent",
                    "date": "2019-07-27T00:00:00.000Z",
                    "amount": -500
                },
                {
                    "accountId": 0,
                    "categoryId": 2,
                    "type": "type.order",
                    "label": "taxes public department: fr123abc456",
                    "rawLabel": "direct debit sepa taxes public department: fr123abc456",
                    "date": "2019-08-17T00:00:00.000Z",
                    "amount": -150
                },
                {
                    "accountId": 0,
                    "categoryId": 3,
                    "type": "type.withdrawal",
                    "label": "ATM my pretty town center",
                    "rawLabel": "debit ATM 18/08/2019 ATM my pretty town center",
                    "date": "2019-08-19T00:00:00.000Z",
                    "amount": -20
                },
                {
                    "accountId": 0,
                    "type": "type.bankfee",
                    "rawLabel": "commission on non euro buy 0.65eur",
                    "date": "2019-08-22T00:00:00.000Z",
                    "amount": -0.65
                }
            ]
        }
    }

## How can I setup notifications with Apprise?

[Apprise](https://github.com/caronc/apprise) is a system that allows to send notifications from an
application to Telegram, Slack, your mobile phone through Gotify, etc.

First, your instance's administrator needs to enable the [Apprise's support]({filename}02-admin.md#).

Then go to the "Alerts" section in Kresus where you'll be able to enter an
[Apprise formatted URL](https://github.com/caronc/apprise/wiki), validate and the configure your
alerts.