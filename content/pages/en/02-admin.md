Title: Administration
Date: 2020-01-02 16:30
Summary: Kresus' administration system guide, from configuration to backups.
Slug: admin
toc_run: true
toc_title: Instance administration
lang: en
status: hidden
is_doc: true

## Configuration

The first thing to do before using Kresus is to configure it so it can work optimally according to
your preferences.

### With a `config.ini` file

Let's generate an empty configuration with the command below.

    :::bash
    ./bin/kresus.js 'create:config' > config.ini

> ⚠️ **Note**: for versions preceding 0.16 this command does not exist. You can however find the list of available options in the file [config.example.ini](https://framagit.org/kresusapp/kresus/raw/master/support/docker/config.example.ini).

Then edit the `config.ini` file and define the options which fit you needs and pass the `-c path/to/config.ini` argument to Kresus upon launch.

**Security :** In production mode (`NODE_ENV=production`), if the configuration file is not readable
only or readable/writable only by its owner, using the filesystem permissions, Kresus will refuse to
start.

### With environment variables

Please note that each configuration option has an associated environment variable: if this variable
is defined, it will override the `INI` config file's value or the default value. Please check the
[`config.ini.example` file](https://framagit.org/kresusapp/kresus/raw/master/support/docker/config.example.ini)
to retrieve all the available environment variables.

## SQL database

Starting from 0.16 it is required to set up a config file (or the associated environment
variables) to define the database configuration options. Please refer to the [exemple file](https://framagit.org/kresusapp/kresus/raw/master/support/docker/config.example.ini) to find out the database options.

The only compatible and production-advised database is [PostgreSQL](https://www.postgresql.org/).

It is **highly recommended to avoid** `sqlite` in production. `sqlite` does not support migrations
really well and can result in data loss. We only recommend its use for tests and development.

If you decide to use `sqlite`,  you'll need to install the package `sqlite3`: `npm install --save sqlite3`.

### Migrate from the legacy system to SQL

Before version 0.16, Kresus used another database system which was not performant. The migration to
SQL allows better performances as well as new horizons for new features.

It is advised to make a [backup](#backup) of your instance before the migration.

#### From 0.15 to .16

If you were using version 0.15 and are upgrading to version 0.16, the data migration should have occurred
automatically upon Kresus' start. Your logs should inform you if this migration did occur. If that's
not the case, please report the [issue](https://framagit.org/kresusapp/kresus/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

#### From < 0.14 included to 0.16

If you were using a version inferior to 0.15 before the SQL migration (e.g. from 0.14 to 0.16) you
can export your data with `kresus-export-cozydb` :

- install the tool:

        :::bash
        npm install -g kresus-export-cozydb


- launch it with a reference to your old configuration file:

        :::bash
        kresus-export-cozydb --config /path/to/old/config.ini --output /tmp/backup.json


    This will read the configuration stored in `/path/to/old/config.ini` and will export your old
    instance to a kresus-specific JSON format in `/tmp/backup.json`.

- in Kresus, use the import tool and select your freshly exported JSON file to re-import all your
  data.

## Set up notifications with Apprise

To enable notifications for your instance users through [Apprise](https://github.com/caronc/apprise/),
it is required to define an [Apprise API server](https://github.com/caronc/apprise-api). In your
configuration file, fill the following line:

    :::INI
    appriseApiBaseUrl=http://localhost:8000/

## Firewall recommendations

You'll need to define the authorizations below:

- HTTP/HTTPS access to your bank's website, to fetch the new transactions.
- HTTP/HTTPS access to the weboob repositories, for the modules automatic update before fetch, if
you use the automatic update feature.

## Instance backup / restoration

For both processes it is highly recommended to shut down Kresus to avoid any modification during the
copy.

### Backup

- Save your configuration file.
- Save the whole content of Kresus' `datadir` directory (as defined in the configuration file).
- Save your database content (starting from 0.16) with the right tool. For example you can use
`pg_dump` for PostgreSQL which will export the given database to a SQL file with all the data in
clear text.

### Restoration

Simply restore the files you previously saved during the previous backup step.
