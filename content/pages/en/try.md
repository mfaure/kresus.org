Title: Try
Date: 2017-07-06 20:00
Slug: demo
Summary: Try
lang: en

# Try Kresus

You can give a try to the latest stable version of Kresus that we host
ourselves. This demo version is preconfigured with accesses to fake bank
accounts and shows fake banking transactions, for the purpose of demonstration.
The data from this demo version is reinitialized every night, to put it back to
a clean state.

<p><strong>Never use real credentials on this demo instance, or any visitor
might see your personal transactions!</strong></p>

<p class="center">
    <a class="button try" href="https://demo.kresus.org" rel="noopener noreferrer" target="_blank">
        Try
    </a>
</p>
