Title: FAQ
Date: 2017-03-11 10:02
Slug: faq
Summary: Foire aux questions : sécurité, comment ça marche, qui sommes-nous...
toc_run: true
toc_title: Foire aux questions
lang: fr
status: hidden
is_doc: True

## Sécurité

### Comment sont stockées mes coordonnées bancaires dans Kresus ?

Si vous utilisez la version autonome de Kresus, il est assumé que vous savez
protéger vos arrières, et notamment sécuriser un minimum votre serveur. Aucune
donnée n'est chiffrée dans ce mode de fonctionnement, pas même votre mot de
passe bancaire ; il vous est donc conseillé de chiffrer le disque sur lequel
est utilisé Kresus, ou encore de containeriser le service web, ou encore
d'utiliser d'autres subterfuges pour chiffrer le mot de passe. Dans le futur,
il est envisageable que le mot de passe soit chiffré dans ce mode de
fonctionnement également.

### Pourquoi dois-je donner mon mot de passe bancaire à Kresus ?

Pour récupérer vos opérations bancaires sur le site de votre banque, Kresus se
fait passer pour un navigateur web (par l'intermédiaire de Weboob), puis se
connecte au site de votre banque à l'aide de votre login et de votre mot de
passe. Cela permet de rapatrier vos opérations bancaires, de manière
automatique, toutes les nuits, sans aucune intervention nécessaire de votre
part.

### Pourquoi Kresus ne supporte pas l'authentification à deux facteurs ?

Comme Kresus se connecte à votre banque de la même façon que vous le feriez
avec votre navigateur, mais qu'il le fait chaque nuit, il faudrait qu'il vous
demande à chaque fois de saisir votre identifiant pour le second facteur, ce
qui pose bien évidemment problème : Kresus ne serait plus autonome.

## Synchronisation avec la banque

### Quand et comment Kresus met-il à jour mes opérations ?

Lors de la création d'un accès Kresus télécharge depuis le site de votre banque l'intégralité des
opérations disponibles.

Par la suite, Kresus télécharge durant la nuit (l'heure varie) les nouvelles
opérations. Cependant, les banques ajoutent parfois les opérations plusieurs jours après leur date
réelle, donc Kresus va récupérer les transactions jusqu'à un mois avant la dernière récupération par
sécurité. Vous pouvez configurer ce seuil de sécurité dans l'administration si vous trouvez qu'un
mois n'est pas suffisant.

## Au sujet de Kresus

### J'ai une question qui n'est pas dans cette Foire Aux Questions et qui devrait l'être, à mon avis.

Super ! N'hésitez-pas à venir nous la poser sur le
[forum](http://community.kresus.org/) ou à [ouvrir un
ticket](https://framagit.org/kresusapp/kresus.org/issues/new) sur le bug
tracker du site web directement. Si vous avez une ébauche de réponse, c'est
encore mieux ! Si vous vous la posez, il y a des chances que vous ne soyez pas
le/la seul⋅e !

### Comment rapporter un bug ?

Kresus est fait par des humains, et à ce titre peut contenir des erreurs ! Si
jamais vous rencontrez un problème lors de votre utilisation de Kresus,
n'hésitez-pas à [ouvrir un
ticket](https://framagit.org/kresusapp/kresus/issues/new) dans notre bug
tracker. Dans votre ticket, essayez d'inclure le plus d'informations possible
sur votre configuration : quelle version de Kresus utilisez-vous ? Quelle
version de Weboob utilisez-vous ? Quelles sont toutes les étapes nécessaires à
la reproduction du bug ? Plus nous aurons d'informations, plus les chances que
nous résolvions votre bug rapidement seront élevées.

### J'aimerais une nouvelle fonctionnalité.

Les idées ne manquent pas et sont toujours appréciées ! Vous pouvez également
[ouvrir un ticket](https://framagit.org/kresusapp/kresus.org/issues/new) pour
proposer une fonctionnalité, et par la suite voire même commencer à
l'implémenter ! Les mainteneurs seront ravis de vous aider à la mettre en
œuvre.
