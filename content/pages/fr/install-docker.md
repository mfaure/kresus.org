Title: Installation via Docker
Date: 2017-03-11 10:02
Slug: install-docker
Summary: Apprenez à installer Kresus via Docker
toc_run: true
toc_title: Comment installer Kresus via Docker&nbsp;?
lang: fr
status: hidden

## Attention : à lire avant de lancer une image docker (pré-construite ou non)

Par défaut docker [expose le port 9876 à
`0.0.0.0`](https://docs.docker.com/engine/userguide/networking/default_network/binding/),
rendant ainsi votre Kresus visible par tous.  Si vous êtes derrière un
reverse-proxy tel que nginx ou apache, préférez `-p 127.0.0.1:9876:9876` pour
rendre visible Kresus uniquement à votre reverse-proxy.

Cette documentation utilisera cependant `-p 9876:9876` dans tous les exemples
ci-dessous, afin de rester générique.

## Lancez une image pré-construite

L'image Docker a l'avantage d'inclure toutes les dépendances nécessaires, dont
une installation de Weboob complète. A chaque redémarrage de l'image, Weboob
essayera de se mettre à jour ; si vous rencontrez donc un problème de modules,
il est recommandé de simplement redémarrer l'image (un simple `restart`
suffit).

L'image pré-construite expose plusieurs volumes de données. Il n'est pas
obligatoire de les monter, mais cela vous permet de conserver vos données
personnelles entre les redémarrages de l'image Docker.

- `/home/user/data` contient toutes les données utilisées par Kresus. Il est
  recommandé de monter ce volume, pour éviter des pertes de données entre
  chaque nouveau démarrage de l'image (par exemple, après une mise à jour).
- `/weboob` contient le clone local de Weboob installé au démarrage. L'exposer
  au système local n'est pas obligatoire, mais permet de :
    - mutualiser les répertoires d'installation de Weboob, si plusieurs images
      Kresus sont instanciées. Cela permet notamment des économies d'espace
      disque.
    - mettre en place des `crontab` sur la machine hôte, qui se chargeront de
      mettre à jour Weboob régulièrement (avec un `git pull` depuis le
      répertoire de Weboob sur l'hôte).
- `/opt/config.ini` contient le fichier de configuration de kresus. Vous devrez au moins renseigner
  la configuration de la base de données à utiliser. Voir plus d'explications sur [le fichier de
  configuration]({filename}02-admin.md#avec-un-fichier-configini).

**Note** : si vous clonez Weboob, il vous est suggéré d'utiliser la branche
*master* (développement) qui devrait être plus à jour que la branche *stable*.

La variable d'environnement suivante peut être définie :

- `LOCAL_USER_ID` permet de choisir l'UID de l'utilisateur interne de l'image
  Docker (afin de ne pas faire que kresus tourne en root dans l'image).

Voici un exemple de ligne de commande pour lancer Kresus en production dans une
image Docker, avec le même utilisateur UNIX que l'actuel :

    :::bash
    mkdir -p /opt/kresus/data
    mkdir -p /opt/kresus/weboob
    touch /opt/kresus/config.ini
    # Éditez /opt/kresus/config.ini en remplissant la configuration de la base de données
    git clone https://git.weboob.org/weboob/weboob.git /opt/kresus/weboob

    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        -v /etc/localtime:/etc/localtime \
        --name kresus \
        -ti -d bnjbvr/kresus

## Utilisation de docker-compose

Le répertoire docker-compose comprend tous les éléments pour lancer facilement
kresus :

- un reverse proxy ([traefik](https://traefik.io/))
- un serveur postfix pour envoyer les mails
- kresus

Les fonctionnalités sont :

- chiffrement ssl automatique avec [letsencrypt](https://letsencrypt.org/).
- protection par login/mot de passe.

Prérequis :

- un serveur public avec un nom de domaine (FQDN), avec les ports 80 et 443
  ouverts (pas de apache ou de nginx).
- avoir [docker-compose](https://docs.docker.com/compose/install/) installé
  (version >= 1.19).

### Méthodologie

- générer un password avec htpasswd :

        :::bash
        htpasswd -bn my_user my_password |awk -F':' '{print $2}'

- renommer le fichier `docker-compose-example.env` en `.env` (il doit rester
  dans le répertoire docker-compose).
- éditer le fichier `.env` en remplaçant toutes les variables d'environnement
  par des valeurs adéquates :
    - `MY_DOMAIN` : le nom complet du serveur (ex : mamachine.mondomaine.net)
    - `MAIL_ADDRESS` : adresse mail pour recevoir les alertes letsencrypt
    - `MY_USERNAME` : nom d'utilisateur pour l'authentification
    - `MY_PASSWD` : mot de passe pour l'authentification, obtenu lors de la
      première étape
- renommer le fichier docker-compose.example.yml en docker-compose.yml
- lancer docker-compose:

        :::bash
        docker-compose up -d

## Construire l'image soi-même

### Image stable

L'image correspondant à la dernière version stable de Kresus peut être
téléchargée via le *hub* de Docker. Il s'agit de l'image accessible via
`bnjbvr/kresus`.

Il est possible de la reconstruire à la main. Vous aurez besoin de `nodejs` (de
préférence la version LTS ou une version plus récente) et de `npm` pour
reconstruire l'image de zéro.

    :::bash
    git clone https://framagit.org/kresusapp/kresus && cd kresus
    docker build -t myself/kresus -f docker/Dockerfile-stable .

Vous pouvez ensuite l'utiliser:

    :::bash
    docker run -p 9876:9876 -v /opt/kresus/data:/home/user/data -ti -d myself/kresus

### Image _Nightly_

Il existe aussi une image _Nightly_, avec les derniers changements, construite
chaque nuit à partir du dépôt Git. Attention, cette image est expérimentale et
peut contenir de nombreux bugs ou corrompre vos données, car la branche
_master_ peut être instable de temps en temps. Cette image récupèrera les
dernières sources depuis le dépôt Git en ligne et n'utilisera donc pas de
sources locales.

Pour construire une image _Nightly_ compilée pour une utilisation en
production (à partir de la version 0.16):

    :::bash
    ./scripts/docker/nightly.sh

Ou avant la version 0.16:

    :::bash
    make docker-nightly

Cette commande construira une image nommée `bnjbvr/kresus-nightly`.

Pour la lancer ensuite, vous pouvez utiliser la même commande `docker run` que
précédemment, en adaptant le nom de l'image:

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        --restart unless-stopped \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -v /opt/kresus/config.ini:/opt/config.ini \
        -v /etc/localtime:/etc/localtime \
        --name kresus \
        -ti -d bnjbvr/kresus-nightly
