Title: Utilisation
Date: 2019-06-17 18:02
Slug: user-doc
Summary: Apprenez à utiliser Kresus le plus efficacement possible !
toc_run: true
toc_title: Comment utiliser Kresus&nbsp;?
lang: fr
status: hidden
is_doc: true

### Le solde d'un de mes comptes bancaires n'est pas correct dans Kresus. Que dois-je faire ?

Cela peut arriver parce que le logiciel utilisé par Kresus, Weboob, donne un
instantané des données bancaires présentes sur le site de votre banque, alors
que Kresus essaie de reconstruire un film entier. Si les données fournies par
votre banque ne sont pas cohérentes au cours du temps (une donnée change),
alors le solde d'un de vos comptes risque d'être incorrect dans Kresus.

Il est conseillé de regarder dans la section "Doublons" de Kresus. Si certaines
opérations semblent être des doublons, cette page vous les présentera et il
vous sera possible de les fusionner, à la main, pour retrouver le solde
correct. Cependant, la manière de faire est rudimentaire et certaines
opérations marquées comme doublons pourraient en fait ne pas en être. Il n'est
pas possible d'annuler la fusion de deux opérations, donc soyez prudents lors
de la suppression des doublons !

Dans le futur, il sera possible d'annuler une fusion de doublons. La détection
automatique de doublons devrait également être grandement améliorée.

Enfin en dernier recours, vous pouvez forcer Kresus à resynchroniser la balance du compte fautif
avec celle retournée par votre banque.
Allez dans *Paramètres* > *Comptes bancaires* et cliquez sur l'engrenage sur la ligne du compte
voulu.

![Resync accountbutton](../images/pages/en/resync-account-icon.png)

Une modale va vous demander de confirmer :

![Modale asking to confirm account resync](../images/pages/fr/resync-confirm-modale.png)

### Comment puis-je ajouter manuellement une opération ?

Il peut être parfois utile de pouvoir renseigner des dépenses à l’avance, avant même
qu’elles n'apparaissent sur le site de notre banque et qu’elles ne soient
récupérées par Kresus.

Pour ajouter manuellement une opération, il suffit d’aller dans la liste des opérations et
de cliquer sur le bouton "Ajouter une opération" ("+" sur mobile).

![Bouton d'ajout d'opération](../images/pages/fr/ajout-op-bouton.png)

Si vous créez cette opération sur un compte qui est mis à jour automatiquement
en se connectant au site de votre banque, lorsque la véritable opération
apparaîtra sur votre compte bancaire et sera récupérée par Kresus, il y aura
un doublon créé, que vous devrez fusionner manuellement. Vous aurez
éventuellement à utiliser le bouton “En trouver plus” si la date de l’opération
manuelle est très différente de la date de l’opération réelle.

![Fenêtre d'ajout d'opération](../images/pages/fr/ajout-op-modale.png)

## Puis-je importer de l'OFX ou d'autres formats ?

Kresus prend en charge le format OFX et son propre format, qui consiste en un fichier JSON avec une entrée `data` qui contient :

* un tableau (array) `accesses` avec les propriétés suivantes :
    * `id` (number): un identifiant unique décrivant l'accès bancaire.
    * `vendorId` (string): l'identifiant du module bancaire/backend. Actuellement soit `manual` soit un des identifiants (`uuid`) d'un module weboob parmi ceux listés ici : [https://framagit.org/kresusapp/kresus/blob/master/shared/banks.json](https://framagit.org/kresusapp/kresus/blob/master/shared/banks.json) (ex: n26, bnporc, etc.). Si l'accès n'est pas supporté par weboob, utilisez `manual`.
    * `login` (string): votre identifiant d'accès. Le mot de passe ne peut pas être défini dans ce format, vous devrez le saisir manuellement dans l'interface.
    * `customLabel` (string/optionnel): un libellé personnalisé (ce que vous voulez).
* un tableau (array) `accounts` avec les propriétés suivantes :
    * `id` (number): un identifiant unique décrivant le compte.
    * `vendorAccountId` (string): une chaîne unique décrivant le compte.
    * `vendorId` (string): l'identifiant du module bancaire/backend. (Oui vous devez le spécifier ici aussi pour le moment).
    * `accessId` (number): une référence à la propriété `id` d'une entrée `accesses`.
    * `type` (string): le type de compte. Une des valeurs de `name` ici : [https://framagit.org/kresusapp/kresus/blob/master/shared/account-types.json](https://framagit.org/kresusapp/kresus/blob/master/shared/account-types.json).
    * `initialBalance` (number): la balance initiale du compte. Si votre historique est incomplet vous pouvez spécifier la balance en l'état qu'elle était avant toutes les opérations importées afin que le total corresponde à la balance actuelle.
    * `label` (string): le libellé du compte.
    * `iban` (string/optionnel): l'IBAN du compte.
    * `currency` (string/optionnel): la devise du compte au format ISO-4217.
* un tableau (array) `categories` avec les propriétés suivantes :
    * `id` (number): un identifiant unique décrivant la catégorie.
    * `label` (string): le libellé de la catégorie.
    * `color` (string/optionnel): la couleur de la catégorie, au format hexadécimal. Si absent, une couleur sera générée automatiquement.
* un tableau (array) `operations` avec les propriétés suivantes :
    * `accountId` (number): une référence à la propriété `id` d'une entrée `accounts`.
    * `categoryId` (number/optionnel): une référence à la propriété `id` d'une entrée `categories`.
    * `type` (string/optionnel): le type de transaction. Une des valeurs de `name` parmi [https://framagit.org/kresusapp/kresus/blob/master/shared/operation-types.json](https://framagit.org/kresusapp/kresus/blob/master/shared/operation-types.json).
    * `rawLabel` (string): le libellé brut de la transaction (généralement le libellé complet).
    * `label` (string/optionnel): le libellé de la transaction (généralement sans métadonnées comme la date ou le type).
    * `customLabel` (string/optionnel): un libellé personnalisé si vous souhaitez quelque chose de plus clair (ex: "Bouffe pour mon anniv").
    * `date` (string): la date de la transaction au format ISO.
    * `debit_date` (string/optionnel): la date de débit (lorsque la banque a pris en compte la transaction) de la transaction au format ISO.
    * `amount` (number): le montant de la transaction.

**Exemple**:

    :::json
    {
        "data": {
            "accesses": [
                {
                    "vendorId": "manual",
                    "login": "whatever-manual-acc--does-not-care",
                    "customLabel": "Optional custom label",
                    "id": 0
                }
            ],
            "accounts": [
                {
                    "id": 0,
                    "vendorId": "manual",
                    "accessId": 0,
                    "vendorAccountId": "manualaccount-randomid",
                    "type": "account-type.checking",
                    "initialBalance": 0,
                    "label": "Compte Courant",
                    "iban": "FR4830066645148131544778523",
                    "currency": "EUR"
                }
            ],
            "categories": [
                {
                    "label": "Groceries",
                    "color": "#1b9d68",
                    "id": 0
                },
                {
                    "label": "Books",
                    "color": "#b562bf",
                    "id": 1
                },
                {
                    "label": "Taxes",
                    "color": "#ff0000",
                    "id": 2
                },
                {
                    "label": "Misc",
                    "color": "#00ff00",
                    "id": 3
                }
            ],
            "operations": [
                {
                    "accountId": 0,
                    "categoryId": 0,
                    "type": "type.card",
                    "label": "Wholemart",
                    "rawLabel": "card 07/07/2019 wholemart",
                    "customLabel": "Food",
                    "date": "2019-07-07T00:00:00.000Z",
                    "amount": -83.8
                },
                {
                    "accountId": 0,
                    "categoryId": 0,
                    "type": "type.card",
                    "label": "Wholemart",
                    "rawLabel": "card 09/07/2019 wholemart",
                    "customLabel": "Food & stuff",
                    "date": "2019-07-09T00:00:00.000Z",
                    "amount": -60.8
                },
                {
                    "accountId": 0,
                    "categoryId": 1,
                    "type": "type.card",
                    "label": "amazon payments",
                    "rawLabel": "carte 19/07/2019 amazon payments",
                    "customLabel": "1984 - George Orwell",
                    "date": "2019-07-19T00:00:00.000Z",
                    "amount": -20
                },
                {
                    "accountId": 0,
                    "type": "type.transfer",
                    "label": "SEPA m. john doe 123457689 rent",
                    "rawLabel": "transfer to m. john doe 123457689 rent",
                    "date": "2019-07-27T00:00:00.000Z",
                    "amount": -500
                },
                {
                    "accountId": 0,
                    "categoryId": 2,
                    "type": "type.order",
                    "label": "taxes public department: fr123abc456",
                    "rawLabel": "direct debit sepa taxes public department: fr123abc456",
                    "date": "2019-08-17T00:00:00.000Z",
                    "amount": -150
                },
                {
                    "accountId": 0,
                    "categoryId": 3,
                    "type": "type.withdrawal",
                    "label": "ATM my pretty town center",
                    "rawLabel": "debit ATM 18/08/2019 ATM my pretty town center",
                    "date": "2019-08-19T00:00:00.000Z",
                    "amount": -20
                },
                {
                    "accountId": 0,
                    "type": "type.bankfee",
                    "rawLabel": "commission on non euro buy 0.65eur",
                    "date": "2019-08-22T00:00:00.000Z",
                    "amount": -0.65
                }
            ]
        }
    }

## Comment puis-je configurer mes notifications avec Apprise ?

[Apprise](https://github.com/caronc/apprise) est un système qui permet
d'envoyer des notifications depuis une application vers Telegram, Slack, votre
mobile via Gotify, etc.

Tout d'abord, il faut que l'administrateur.ice de votre instance ait [mis en
place le support
d'Apprise]({filename}02-admin.md#mettre-en-place-les-notifications-avec-apprise_2).

Ensuite, rendez-vous dans la section "Alertes" de Kresus, où vous pourrez
rentrer une URL au [format Apprise](https://github.com/caronc/apprise/wiki),
valider, et ensuite configurer vos alertes.
