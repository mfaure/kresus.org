Title: Essayer
Date: 2017-07-06 20:00
Slug: demo
Summary: Essayer
lang: fr

# Essayer Kresus

Vous avez la possibilité d'essayer la dernière version stable de Kresus,
hébergée par nos soins. Cette version de démonstration est pré-configurée avec
des accès à des faux comptes en banque, présentant également des fausses
transactions bancaires. Les données présentes sont réinitialisées toutes les
nuits pour remettre la démo dans un état "propre".

<p><strong>N'utilisez surtout pas vos vrais logins et mot de passes sur cette instance de
démonstration, sinon n'importe quel visiteur pourra voir vos transactions
personnelles !</strong></p>

<p class="center">
    <a class="button try" href="https://demo.kresus.org" rel="noopener noreferrer" target="_blank">
        Essayer
    </a>
</p>
